<?php
session_start();
include "../config.php";

$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

// include "../on-member/headmember.php";

?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../style/css/bootstrap.css" rel="stylesheet">
    <link href="../style/style.css" rel="stylesheet">
    <link href="../style/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <title>Koperasi Radar Bandung</title>
</head>
<div class="container-fluid">

    <div class="d-sm-flex align-items-center justify-content-between m-4">
        <h1 class="h3 mb-0 text-gray-800">Data Penjualan Barang</h1>
    </div>

    <div class=" col-xl-12 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-dark">Input Data Baru</h6>
            </div>
            <form class="col-12 mt-3">
                <table class="table table-bordered table-hover table-striped">
                    <thead class=" bg-light text-dark">
                        <tr>
                            <th scope="col" class="row-md-0">No</th>
                            <th scope="col" class="row-md-0">ID Jual</th>
                            <th scope="col" class="row-md-5">Nama Petugas</th>
                            <th scope="col" class="row-md-0">ID-PLG</th>
                            <th scope="col">Nama Pelanggan</th>
                            <th scope="col">ID Barang</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col">Ket</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
include '../config.php';
$no = 1;
$data = mysqli_query($koneksi, "select * from tb_penjualan");
while ($d = mysqli_fetch_array($data)) {
    ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $d['id_jual']; ?></td>
                            <td><?php echo $d['nm_petugas']; ?></td>
                            <td><?php echo $d['id_plg']; ?></td>
                            <td><?php echo $d['nm_plg']; ?></td>
                            <td><?php echo $d['id_brg']; ?></td>
                            <td><?php echo $d['nm_brg']; ?></td>
                            <td><?php echo $d['jumlah']; ?></td>
                            <td><?php echo $d['ket']; ?></td>
                            <td><?php echo $d['tanggal']; ?></td>
                            <td>Rp. <?php echo $d['total']; ?></td>
                        </tr>
                        <?php
}
?>
                    </tbody>
                </table>
        </div>
        </form>
    </div>
</div>
<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

<script>
window.print();
</script>


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

</body>

<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../style/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="../vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="../style/js/demo/chart-area-demo.js"></script>
<script src="../style/js/demo/chart-pie-demo.js"></script>

</html>