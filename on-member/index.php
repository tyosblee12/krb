<?php
session_start();
include "../config.php";

$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

$databarang = mysqli_query($koneksi, "SELECT * FROM tb_barang");
$jumlahbarang = mysqli_num_rows($databarang);

$datapenjualan = mysqli_query($koneksi, "SELECT * FROM tb_penjualan");
$hasiljual = mysqli_num_rows($datapenjualan);
// SUBTOTAL
$barang = mysqli_fetch_array(mysqli_query($koneksi, "SELECT id_jual, nm_petugas, nm_plg, id_brg,
                    nm_brg,jumlah, tanggal, SUM(total) as Total_Bayar FROM `tb_penjualan` "));
$data_barang = array('Total_Bayar' => $barang['Total_Bayar']);

$kode = mysqli_query($koneksi, "SELECT * FROM tb_penjualan");
$kodeotomatis = mysqli_num_rows($kode);
$akhir = $kodeotomatis;

include "headmember.php";
?>
<!-- ============================= AKHIR DARI BAR ATAS ============================= -->
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard Order Barang</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4 animated--grow-in">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                Penghasilan (Annual)</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                Rp. <?php echo $data_barang['Total_Bayar']; ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-wallet fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4 animated--grow-in">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Data Order</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $hasiljual; ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dolly fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4 animated--grow-in">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah Barang
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                        <?php echo $jumlahbarang; ?> Buah</div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info progress-bar-animated progress-bar-striped"
                                            role="progressbar" style="width: <?php echo $jumlahbarang; ?>0%"
                                            aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-box-open fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4 animated--grow-in">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Pending Requests</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row animated--grow-in">
        <!-- COBA -->
        <div class="col-xl-6 col-lg-7">
            <div class="card shadow mb-4 ">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-dark">Input Data Baru</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <form action="tpenjual.php" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="namapetugas">ID JUAL</label>
                                <input type="text" class="form-control" id="id_jual" name="id_jual"
                                    value="50<?php echo $akhir; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="namapetugas">Petugas</label>
                                <input type="text" class="form-control" name="nm_petugas" id="nm_petugas"
                                    value="<?php echo $data['nama']; ?>" readonly>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label for="inputPassword4">ID Pelanggan</label>
                                <select onchange='cek_dataplg()' id='id_plg' name="id_plg" class="form-control">
                                    <option value='' selected>- Pilih -</option>
                                    <?php
include "../config.php";
$plg = mysqli_query($koneksi, "SELECT * FROM tb_pelanggan");
while ($row = mysqli_fetch_array($plg)) {
    echo "<option value='$row[id_plg]'>$row[id_plg] - $row[nama_plg]</option>";
}
?>
                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="inputPassword4">Nama Pelanggan</label>
                                <input type="text" class="form-control" name="nama_plg" id="nama_plg" readonly>
                            </div>
                        </div>
                        <script type="text/javascript">
                        function cek_dataplg() {
                            var id = $("#id_plg").val();
                            $.ajax({
                                url: '../on-admin/ambildataplg.php',
                                data: "id_plg=" + id,
                            }).success(function(data) {
                                var json = data,
                                    obj = JSON.parse(json);
                                $('#nama_plg').val(obj.nama_plg);
                            });
                        }
                        </script>

                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="inputAddress">ID Barang </label>
                                <select onchange='cek_database()' id='id_brg' name="id_brg" class="form-control">
                                    <option value='' selected>- Pilih -</option>
                                    <?php
include "../config.php";
$barang = mysqli_query($koneksi, "SELECT * FROM tb_barang");
while ($row = mysqli_fetch_array($barang)) {
    echo "<option value='$row[id_brg]'>$row[id_brg]</option>";
}
?>
                                </select>
                            </div>
                            <div class="form-group col-md-10">
                                <label for="inputAddress">Nama Barang </label>
                                <input type="text" class="form-control" id="nama_brg" name="nama_brg" readonly>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="inputCity">Harga</label>
                                <input type="text" class="form-control text-right" id="harga" readonly>
                            </div>
                            <script type="text/javascript">
                            function cek_database() {
                                var id = $("#id_brg").val();
                                $.ajax({
                                    url: '../on-admin/ambildatabrg.php',
                                    data: "id_brg=" + id,
                                }).success(function(data) {
                                    var json = data,
                                        obj = JSON.parse(json);
                                    $('#nama_brg').val(obj.nama_brg);
                                    $('#harga').val(obj.harga);
                                });
                            }
                            </script>
                            <div class="form-group col-md-5 font-weight-bold text-danger">
                                <label for="inputCity"> * Jumlah</label>
                                <input type="text" class="form-control text-right" id="jumlah" name="jumlah"
                                    onchange="totalharga()">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4 font-weight-bold">
                                <label for="inputCity ">Total</label>
                                <input type="text" class="form-control font-weight-bold" id="total" name="total"
                                    value="Rp. " readonly>
                            </div>
                            <div class="form-group col-md-8 font-weight-bold">
                                <label for="inputCity ">Keterangan</label>
                                <select class="custom-select" name="ket">
                                    <option value='' selected>- Pilih -</option>
                                    <option value='Kasbon'>Kasbon</option>
                                    <option value='Cash'>Cash</option>
                                </select>
                            </div>
                        </div>
                        <script>
                        var harga = document.getElementById("harga");
                        var jumlah = document.getElementById("jumlah");
                        var total = document.getElementById("total");

                        function totalharga() {
                            total.value = Number(harga.value) * Number(jumlah.value);
                        }
                        </script>
                        <div class="form-group">
                            <label for="example-date-input">Tanggal Order</label>
                            <div class="col-13">
                                <input class="form-control" type="date" name="tanggal" id="tanggal">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" name="submit">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- END OF COBA -->

        <div class="col-xl-6 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-dark">Daftar Stok Barang</h6>
                </div>
                <form class="col-12 ">
                    <table class="table table-bordered table-hover table-striped mt-2">
                        <thead>
                            <tr class="bg-info text-white">
                                <th scope="col">No</th>
                                <th scope="col">Kode Barang</th>
                                <th scope="col">Nama Barang</th>
                                <th scope="col">Stock</th>
                                <th scope="col">Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
include '../config.php';
$no = 1;
$data = mysqli_query($koneksi, "select * from tb_barang");
while ($d = mysqli_fetch_array($data)) {
    ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $d['id_brg']; ?></td>
                                <td><?php echo $d['nama_brg']; ?></td>
                                <td><?php echo $d['stock']; ?></td>
                                <td>Rp. <?php echo $d['harga']; ?></td>
                            </tr>
                            <?php
}
?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>

        <div class=" col-xl-12 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-dark">Input Data Baru</h6>
                </div>
                <form class="col-12">
                    <table class="table table-bordered table-hover table-striped">
                        <thead class=" bg-info text-white">
                            <tr>
                                <th scope="col" class="row-md-0">No</th>
                                <th scope="col" class="row-md-0">ID Jual</th>
                                <th scope="col" class="row-md-5">Nama Petugas</th>
                                <th scope="col" class="row-md-0">ID-PLG</th>
                                <th scope="col">Nama Pelanggan</th>
                                <th scope="col">ID Barang</th>
                                <th scope="col">Nama Barang</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">Ket</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
include '../config.php';
$no = 1;
$data = mysqli_query($koneksi, "select * from tb_penjualan");
while ($d = mysqli_fetch_array($data)) {
    ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $d['id_jual']; ?></td>
                                <td><?php echo $d['nm_petugas']; ?></td>
                                <td><?php echo $d['id_plg']; ?></td>
                                <td><?php echo $d['nm_plg']; ?></td>
                                <td><?php echo $d['id_brg']; ?></td>
                                <td><?php echo $d['nm_brg']; ?></td>
                                <td><?php echo $d['jumlah']; ?></td>
                                <td><?php echo $d['ket']; ?></td>
                                <td><?php echo $d['tanggal']; ?></td>
                                <td>Rp. <?php echo $d['total']; ?></td>
                            </tr>
                            <?php
}
?>
                        </tbody>
                    </table>
            </div>
            </form>
        </div>
    </div>

    <!-- TABLE DATA BARU -->

    <!-- END OF TABLE DATA BARU -->
    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; Zigma Art Creative Design 2019</span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
</body>

<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../style/js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../style/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<!-- <script src="../vendor/chart.js/Chart.min.js"></script> -->

<!-- Page level custom scripts -->
<!-- <script src="../style/js/demo/chart-area-demo.js"></script>
<script src="../style/js/demo/chart-pie-demo.js"></script> -->

</html>