<?php
session_start();
include "../config.php";

$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

include "../header.php";
$id_user = $_GET['id_user'];
$query_mysqli = $koneksi->query("SELECT * FROM users WHERE id_user='$id_user'") or die(mysqli_error());
// $nomor = 1;
// AMBIL DATA
$dataptg = mysqli_fetch_array(mysqli_query($koneksi, "select * from users where id_user='$_GET[id_user]'"));
$data_ptg = array('nama' => $dataptg['nama'],
    'username' => $dataptg['username'],
    'password' => md5($dataptg['password'],true),
    'level_user' => $dataptg['level_user']);

while ($data = mysqli_fetch_array($query_mysqli)) {
    ?>
<div class=" col-xl-12 col-lg-7">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-dark">Ubah Data Petugas</h6>
        </div>

        <form class="col-md-12 p-3 mb-3" method="post">
            <div class="row">
                <div class="col col-md-1">
                    <label for="id_petugas">ID User</label>
                    <input type="text" class="form-control" name="id_user" placeholder="ID"
                        value=" <?php echo $dataptg['id_user']; ?>" readonly>
                </div>
                <div class="col col-md-3">
                    <label for="id_petugas">Nama Petugas</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama Petugas"
                        value="<?php echo $dataptg['nama']; ?>">
                </div>
                <div class="col col-md-3">
                    <label for="id_petugas">Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Last name"
                        value="<?php echo $dataptg['username']; ?>">
                </div>
                <div class="col col-md-3">
                    <label for="id_petugas">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Last name"
                        value="<?php echo md5($dataptg['password']); ?>">
                </div>
                <div class="col col-md-2">
                    <label for="id_petugas">Level</label>
                    <select class="custom-select" name="level_user">
                        <option value='<?php echo $dataptg['level_user']; ?>' selected>
                            <?php echo $dataptg['level_user']; ?></option>
                        <option value='admin'>Admin</option>
                        <option value='member'>Member</option>
                    </select>
                    <!-- <button type="reset" class="btn btn btn-danger mt-3 float-right ml-3" value="Reset"> Reset</button> -->
                    <a href="dpetugas.php" class="btn btn-danger mt-3 float-right ml-2" role="button"
                        aria-disabled="true">Reset</a>
                    <!-- <button type="submit" class="btn btn-danger mt-3 float-right ml-3">Batal</button> -->
                    <button type="submit" class="btn btn-warning mt-3 float-right" name="submit"> Ubah</button>
                </div>
            </div>
        </form>
        <?php }?>
    </div>
</div>

<?php
include '../config.php';
if (isset($_POST['submit'])) {
    try {
        $id_user = $_POST['id_user'];
        $nama = $_POST['nama'];
        $username = $_POST['username'];
        $password = md5($_POST['password']);
        $level_user = $_POST['level_user'];

        mysqli_query($koneksi, "UPDATE users SET id_user='$id_user', nama='$nama', username='$username', password='$password', level_user='$level_user' WHERE id_user='$id_user'");

        print "<script>alert('Berhasil Mengubah Data')
	window.location = 'dpetugas.php';
	</script>";
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
?>




<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Zigma Art Creative Design 2019</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
</body>
<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../style/js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../style/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<!-- <script src="../vendor/chart.js/Chart.min.js"></script> -->

<!-- Page level custom scripts -->
<!-- <script src="../style/js/demo/chart-area-demo.js"></script>
<script src="../style/js/demo/chart-pie-demo.js"></script> -->

</html>