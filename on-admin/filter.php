<?php
session_start();
include "../config.php";

$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

include_once "../header.php"
// SQL TAMBAH DATA

?>

<div class=" col-xl-12 col-lg-7">
    <div class="card shadow mb-4">
        <!-- SEARCH -->
        <script>
        (function(document) {
            'use strict';

            var TableFilter = (function(myArray) {
                var search_input;

                function _onInputSearch(e) {
                    search_input = e.target;
                    var tables = document.getElementsByClassName(search_input.getAttribute(
                        'data-table'));
                    myArray.forEach.call(tables, function(table) {
                        myArray.forEach.call(table.tBodies, function(tbody) {
                            myArray.forEach.call(tbody.rows, function(row) {
                                var text_content = row.textContent
                                    .toLowerCase();
                                var search_val = search_input.value
                                    .toLowerCase();
                                row.style.display = text_content
                                    .indexOf(search_val) > -1 ? '' :
                                    'none';
                            });
                        });
                    });
                }

                return {
                    init: function() {
                        var inputs = document.getElementsByClassName('search-input');
                        myArray.forEach.call(inputs, function(input) {
                            input.oninput = _onInputSearch;
                        });
                    }
                };
            })(Array.prototype);

            document.addEventListener('readystatechange', function() {
                if (document.readyState === 'complete') {
                    TableFilter.init();
                }
            });

        })(document);
        </script>
        <!-- SEARCH -->

        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-dark">Tabel Hasil Filter</h6>
        </div>
        <form class="col-12 mt-3">
            <div class="col mr-2">
                <div class="input-group">
                    <input type="search" class="form-control bg-light border-1 small col-md-2 search-input" id="search"
                        placeholder="Search for..." data-table="penjualan">
                </div>
                <br>
            </div>
            <table class="table table-bordered table-hover table-striped penjualan">
                <thead class=" bg-info text-white">
                    <tr>
                        <th scope="col" class="row-md-0">No</th>
                        <th scope="col" class="row-md-0">ID Jual</th>
                        <th scope="col" class="row-md-5">Nama Petugas</th>
                        <th scope="col" class="row-md-0">ID-PLG</th>
                        <th scope="col">Nama Pelanggan</th>
                        <th scope="col">ID Barang</th>
                        <th scope="col">Nama Barang</th>
                        <th scope="col">Jumlah</th>
                        <th scope="col">Ket</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Total</th>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
// var_dump($_POST);
if (isset($_POST['submit'])) {
    $no = 1;
    $id_plg = $_POST['id_plg'];
    $ket = $_POST['ket'];
// AMBIL TOTAL
    $barang = mysqli_fetch_array(mysqli_query($koneksi, "SELECT SUM(total) as Total_Bayar FROM `tb_penjualan`
    WHERE ket like '%$ket%' and id_plg = '$id_plg' "));
    $data1 = array('Total_Bayar' => $barang['Total_Bayar']);
// AKHIR AMBIL TOTAL

    $data = mysqli_query($koneksi, "SELECT tb_penjualan.id_jual, tb_penjualan.nm_petugas, tb_penjualan.id_plg, tb_pelanggan.nama_plg, tb_penjualan.id_brg, tb_barang.nama_brg, tb_penjualan.jumlah, tb_penjualan.ket, tb_penjualan.tanggal , tb_penjualan.total FROM tb_penjualan, tb_pelanggan, tb_barang 
    where tb_penjualan.id_plg = tb_pelanggan.id_plg AND tb_penjualan.id_brg = tb_barang.id_brg and tb_penjualan.id_plg like '%$id_plg%' and tb_penjualan.ket like '%$ket%' ");
    while ($d = mysqli_fetch_array($data)) {
        ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $d['id_jual']; ?></td>
                        <td><?php echo $d['nm_petugas']; ?></td>
                        <td><?php echo $d['id_plg']; ?></td>
                        <td><?php echo $d['nama_plg']; ?></td>
                        <td><?php echo $d['id_brg']; ?></td>
                        <td><?php echo $d['nama_brg']; ?></td>
                        <td><?php echo $d['jumlah']; ?></td>
                        <td><?php echo $d['ket']; ?></td>
                        <td><?php echo $d['tanggal']; ?></td>
                        <td>Rp. <?php echo $d['total']; ?></td>
                    </tr>
                    <?php
}
}
?>
                    <tr class="bg-danger text-white">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Subtotal :</td>
                        <td class="font-weight-bold  "> Rp. <?php echo $data1['Total_Bayar']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <a class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm float-right mb-3" href="dpenjual.php">
                <i class="far fa-trash-alt fa-sm text-black"></i> Kembali </a>
    </div>
    </form>
</div>