<?php
session_start();
include '../config.php';

$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

?>
<!-- INI PENTING UNTUK DASHBOARD -->
<?php
include '../header.php';
?>
<!-- INI PENTING UNTUK DASHBOARD -->
<!-- ============================= AKHIR DARI BAR ATAS ============================= -->
<!-- Begin Page Content -->

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard Data Penjualan Barang</h1>
        <a href="../cetak/cetakjual.php" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i
                class="fas fa-download fa-sm text-dark-50"></i> Cetak Data Penjualan</a>
    </div>

    <!-- TAMBAH PETUGAS -->
    <div class="card shadow mb-4 ">
        <!-- Card Header - Accordion -->
        <a href="#collapseCardExample" class="d-block card-header py-3 bg-info" data-toggle="collapse" role="button"
            aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-white ">Filter Penjualan</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse hide" id="collapseCardExample">
            <div class="card-body">
                <form action="filter.php" method="POST">
                    <div class="row">
                        <div class="col col-md-6">
                            <label for="id_petugas" class="font-weight-bold">ID Pelanggan</label>
                            <select onchange='cek_dataplg()' id='id_plg' name="id_plg" class="form-control">
                                <option value='' selected>- Pilih -</option>
                                <?php
                include '../config.php';
                $plg = mysqli_query($koneksi, 'SELECT * FROM tb_pelanggan');
                while ($row = mysqli_fetch_array($plg)) {
                    echo "<option value='$row[id_plg]'>$row[id_plg] - $row[nama_plg]</option>";
                }
                ?>
                            </select>
                        </div>
                        <script type="text/javascript">
                        function cek_dataplg() {
                            var id = $("#id_plg").val();
                            $.ajax({
                                url: 'ambildataplg.php',
                                data: "id_plg=" + id,
                            }).success(function(data) {
                                var json = data,
                                    obj = JSON.parse(json);
                                $('#nama_plg').val(obj.nama_plg);
                            });
                        }
                        </script>

                        <div class="col col-md-6">
                            <label for="keterangan" class="font-weight-bold text-danger">Keterangan
                                (Cash/Kasbon)</label>
                            <select class="custom-select" name="ket">
                                <option value='' selected>- Pilih -</option>
                                <option value='Kasbon' id='Kasbon'>Kasbon</option>
                                <option value='Cash' id='Cash'>Cash</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info float-right mb-3 mt-3" name="submit">Filter</button>
                </form>
            </div>
        </div>
    </div>
    <!-- END of Filter -->

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-md-6 mb-4 animated--grow-in">
            <div class="card shadow h-100 py-2">
                <div class="card-body">

                    <!-- SEARCH -->
                    <script>
                    (function(document) {
                        'use strict';

                        var TableFilter = (function(myArray) {
                            var search_input;

                            function _onInputSearch(e) {
                                search_input = e.target;
                                var tables = document.getElementsByClassName(search_input.getAttribute(
                                    'data-table'));
                                myArray.forEach.call(tables, function(table) {
                                    myArray.forEach.call(table.tBodies, function(tbody) {
                                        myArray.forEach.call(tbody.rows, function(row) {
                                            var text_content = row.textContent
                                                .toLowerCase();
                                            var search_val = search_input.value
                                                .toLowerCase();
                                            row.style.display = text_content
                                                .indexOf(search_val) > -1 ? '' :
                                                'none';
                                        });
                                    });
                                });
                            }

                            return {
                                init: function() {
                                    var inputs = document.getElementsByClassName('search-input');
                                    myArray.forEach.call(inputs, function(input) {
                                        input.oninput = _onInputSearch;
                                    });
                                }
                            };
                        })(Array.prototype);

                        document.addEventListener('readystatechange', function() {
                            if (document.readyState === 'complete') {
                                TableFilter.init();
                            }
                        });

                    })(document);
                    </script>
                    <!-- SEARCH -->
                    <div class="col mr-2">
                        <div class="input-group">
                            <input type="search" class="form-control bg-light border-1 small col-md-2 search-input"
                                id="search" placeholder="Search for..." data-table="customer-list">
                        </div>
                        <br>
                    </div>
                    <form class="col-12">
                        <table class="table table-bordered table-hover customer-list" id="table">
                            <thead class="thead-dark">
                                <tr class="text-white">
                                    <th scope="col" class="row-md-0">ID Jual</th>
                                    <th scope="col" class="row-md-5">Nama Petugas</th>
                                    <th scope="col" class="row-md-0">ID-PLG</th>
                                    <th scope="col">Nama Pelanggan</th>
                                    <th scope="col">ID Barang</th>
                                    <th scope="col">Nama Barang</th>
                                    <th scope="col">Jumlah</th>
                                    <th scope="col">Ket</th>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
include '../config.php';
$data = mysqli_query($koneksi, 'SELECT tb_penjualan.id_jual, tb_penjualan.nm_petugas, tb_penjualan.id_plg, tb_pelanggan.nama_plg, tb_penjualan.id_brg, tb_barang.nama_brg, tb_penjualan.jumlah, tb_penjualan.ket, tb_penjualan.tanggal , tb_penjualan.total FROM tb_penjualan, tb_pelanggan, tb_barang WHERE tb_penjualan.id_plg = tb_pelanggan.id_plg AND tb_penjualan.id_brg = tb_barang.id_brg');
while ($d = mysqli_fetch_array($data)) {
    ?>
                                <tr>
                                    <td><?php echo $d['id_jual']; ?></td>
                                    <td><?php echo $d['nm_petugas']; ?></td>
                                    <td><?php echo $d['id_plg']; ?></td>
                                    <td><?php echo $d['nama_plg']; ?></td>
                                    <td><?php echo $d['id_brg']; ?></td>
                                    <td><?php echo $d['nama_brg']; ?></td>
                                    <td><?php echo $d['jumlah']; ?></td>
                                    <td><?php echo $d['ket']; ?></td>
                                    <td><?php echo $d['tanggal']; ?></td>
                                    <td>Rp. <?php echo $d['total']; ?></td>
                                </tr>
                                <?php
}
?>
                            </tbody>
                        </table>
                        <a class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm float-right" href="#"
                            data-toggle="modal" data-target="#hapusModal">
                            <i class="far fa-trash-alt fa-sm text-black"></i> Batch Delete </a>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>

<!-- Logout Modal-->
<div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin ingin menghapus semua ?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Pilih <strong> "Ya" </strong> jika sudah yakin.
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger" href="hapusdata.php">Hapus</a>
            </div>
        </div>
    </div>
</div>



<!-- Footer -->
<?php
include '../footer.php';
?>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

</body>

<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../style/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="../vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="../style/js/demo/chart-area-demo.js"></script>
<script src="../style/js/demo/chart-pie-demo.js"></script>

</html>