<?php
session_start();
include "../config.php";

$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

// ID BARANG OTOMATIS
$kode = mysqli_query($koneksi, "SELECT id_plg FROM tb_pelanggan");
while ($d = mysqli_fetch_array($kode)) {
    $kodeotomatis = ($d['id_plg']) + 1;
}

include "../header.php";
?>
<!-- ============================= AKHIR DARI BAR ATAS ============================= -->
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard Data Pelanggan</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i
                class="fas fa-plus fa-sm text-black-50"></i> Tambah Pelanggan</a>
    </div>

    <!-- TAMBAH PETUGAS -->
    <div class="card shadow mb-4 ">
        <!-- Card Header - Accordion -->
        <a href="#collapseCardExample" class="d-block card-header py-3 bg-info" data-toggle="collapse" role="button"
            aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-white ">Tambah Data Pelanggan</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse hide" id="collapseCardExample">
            <div class="card-body">
                <form action="tbarang.php" method="POST">
                    <div class="row">
                        <div class="col col-md-3">
                            <label for="id_petugas">ID Pelanggan</label>
                            <input type="text" class="form-control" name="id_brg" placeholder="ID"
                                value="<?php echo $kodeotomatis; ?>" readonly>
                        </div>
                        <div class="col col-md-3">
                            <label for="id_petugas">Nama Pelanggan</label>
                            <input type="text" class="form-control" name="nama_brg" placeholder="Nama Barang">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary float-right mb-3 mt-3" name="submit">Tambah</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-md-6 mb-4 animated--grow-in">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="input-group">
                                <input type="text" class="form-control bg-light border-1 small col-md-2"
                                    onkeyup="caridata()" id="myInput" placeholder="Cari data Pelanggan">
                            </div>
                            <br>
                        </div>


                        <div class="col-12">
                            <table class="table table-bordered table table-hover table-striped" id="myTable">
                                <thead class="thead-dark ">
                                    <tr>
                                        <th scope="col" class="row-md-2">ID Pelanggan</th>
                                        <th scope="col" class="row-md-7">Nama Pelanggan</th>
                                        <th scope="col" class="row-md-2">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
include '../config.php';
$no = 1;
$data = mysqli_query($koneksi, "select * from tb_pelanggan");
while ($d = mysqli_fetch_array($data)) {
    ?>
                                    <tr>
                                        <!-- <td><?php echo $no++; ?></td> -->
                                        <td><?php echo $d['id_plg']; ?></td>
                                        <td class="font-weight-bold"><?php echo $d['nama_plg']; ?></td>
                                        <td>
                                            <a class="d-none d-sm-inline-block btn btn-sm btn-success"
                                                href="upelanggan.php?id_plg=<?php echo $d['id_plg']; ?>"> <i
                                                    class="fas fa-pen fa-sm text-black mr-1"></i> Ubah</a>
                                            <a class="d-none d-sm-inline-block btn btn-sm btn-danger"
                                                href="hpelanggan.php?id_plg=<?php echo $d['id_plg']; ?>"
                                                data-toggle="modal" data-target="#hapusModal"> <i
                                                    class="fas fa-trash-alt fa-sm text-black mr-1"></i> Hapus</a>
                                        </td>
                                    </tr>
                                    <?php
}
?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yakin Menghapus ?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Pilih <strong> "Hapus" </strong> jika ingin keluar dari sesi ini.
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="hpelanggan.php?id_plg=<?php echo $d['id_plg']; ?>">Hapus</a>
                </div>
            </div>
        </div>
    </div>


    <!-- Footer -->
    <?php
include '../footer.php';
?>
    <!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>


</body>

<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../style/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="../vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="../style/js/demo/chart-area-demo.js"></script>
<script src="../style/js/demo/chart-pie-demo.js"></script>

</html>