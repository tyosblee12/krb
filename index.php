<?php
session_start();

if (isset($_SESSION['username']) && $_SESSION['username'] != '') {
    $halaman = $_SESSION['username'];

    header('location:on-' . $halaman);
    exit();
} else {
    header('location:login.php');
    exit();
}