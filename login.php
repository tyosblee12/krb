<?php
if (isset($_GET['error'])): ?>
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>Warning!</strong><?=base64_decode($_GET['error']);?>
</div>
<?php
// header('location: login.php');
?>

<?php endif;?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KRB - Radar Bandung</title>

    <link href="style/css/bootstrap.css" rel="stylesheet">
    <!-- <link href="style/style.css" rel="stylesheet"> -->
    <!-- NEW CSS -->
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style/css/login1.css">
</head>

<body>
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 px-0 d-none d-sm-block">
                    <img src="style/img/aa37-03.jpg" alt="login image" class="login-img">
                </div>
                <div class="col-sm-4 login-section-wrapper border-left">
                    <div class="brand-wrapper">
                        <img src="style/img/logo_2.png" alt="logo" class="logo">
                    </div>
                    <div class="login-wrapper my-auto ">
                        <h1 class="login-title">Sign into your account</h1>
                        <form action="check-login.php" method="post">
                            <div class="form-group">
                                <label for="email" class="sr-only fa">Username</label>
                                <input type="text" name="username" id="username" class="form-control"
                                    placeholder="Username">
                            </div>
                            <div class="form-group ">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" name="password" id="password" class="form-control"
                                    placeholder="***********">
                            </div>
                            <input type="submit" class="btn btn-block login-btn mb-4" name="submit" value="LOGIN">

                        </form>
                        <a href="#!" class="forgot-password-link">Forgot password?</a>
                    </div>
                </div>
            </div>
    </main>

    <script src="style/js/jquery-3.4.1.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
</body>

</html>